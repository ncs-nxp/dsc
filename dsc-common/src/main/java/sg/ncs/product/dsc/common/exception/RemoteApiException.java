package sg.ncs.product.dsc.common.exception;

public class RemoteApiException extends RuntimeException {

    public RemoteApiException(String message) {
        super(message);
    }
}
